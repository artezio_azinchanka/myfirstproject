"use strict";

var userId;         //для хранения id юзера, подлежащего правке
var users;          //промежуточный массив данных пользователей
var $addEditUser = {};  //объект с данными пользователя для добавления и правки
var validity; //маркер валидности формы

(function typeOfAction() { //извлекаем данные из строки запроса и выясняем тип операции add или edit
    var tmp = [];
    var tmp2 = [];
    var param = {}; //Объект с переданными парами ключ:значение
    var get = location.search;  // строка GET запроса
    if (get != '') { // если действие - правка пользователя
        tmp = (get.substr(1)).split('&');   // разделяем переменные
        for (var i = 0; i < tmp.length; i++) {
            tmp2 = tmp[i].split('=');       // массив param будет содержать пары ключ(имя переменной)->значение
            param[tmp2[0]] = tmp2[1];
        }
        for (var key in param) {
            if (key == 'userId') {
                userId = param[key];
            }
        }
        $(document).ready(function () {
            $("h1").append('Edit form'); // пишем соответствующий заголовок

            $.get(  //запрос на сервер для получения данных о пользователе для правки
                '/api/user/' + userId,
                {userId: userId},  //передаем id пользователя для правки
                function (response) {
                    var userForEdit = response;
                    $("#firstName").val(userForEdit['firstName']);  //вписываем в поля исходные данные для правки
                    $("#lastName").val(userForEdit['lastName']);
                    $("#birthDate").val(userForEdit['birthDate']);
                    if (userForEdit['test'] == 'да') $("#test").prop('checked', true);
                    if (userForEdit['sex'] == 'Мужской') $("#sex").prop('selectedIndex', 1);
                    else $("#sex").prop('selectedIndex', 2);
                    if (userForEdit['place'] == 'Директор') $("#place_dir").prop('checked', true);
                    else if (userForEdit['place'] == 'Менеджер') $("#place_man").prop('checked', true);
                    else $("#place_work").prop('checked', true);
                }
            );
        });
    }
    else { // если действие - добавление нового пользователя
        $(document).ready(function () {
            $("h1").append('Add form'); // пишем соответствующий заголовок
        })
    }
})();

var backToRegistry = function () {          //возвращает нас на исходную страницу
    window.location.href = "index.html";
};

var guid = function () {  //функция генерации guid
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};

var submitForm = function () { //функция добавления\правки пользователя, если форма валидна
    validity = validate();
    if (validity) {                 // если форма валинда
        if (userId == undefined) {  // если в строке браузера не передан id юзера для правки, значит создаем нового юзера
            addUser();
        }
        else {                      // если в строке браузера передан id юзера для правки, правим юзера с указанным id
            editUser();
        }
    }
};

var addUser = function () {  //добавляем нового пользователя
    $addEditUser.id = guid();    //генерируер guid
    $("input[class='form-control']").each(function () { //Получаем данные, введенные пользователем
        $addEditUser[$(this).attr('id')] = $(this).val();
    });
    if ($("#test").prop("checked")) $addEditUser.test = "да";
    else $addEditUser.test = "нет";
    $addEditUser.sex = $("#sex").val();
    $addEditUser.place = $('input[name=place]:checked').val();

    $.ajax({  //запрос на сервер для добавления нового пользователя
        type: 'POST',
        url: '/api/user',
        data: $addEditUser   //данные о новом пользователе
    }).done(function () {
        backToRegistry(); //по окончании вернуться на главную страницу
    });
};

var editUser = function () {  //правим пользователя
    $addEditUser.id = userId; //получаем id пользователя для правки
    $("input[class='form-control']").each(function () { //Получаем данные, введенные пользователем
        $addEditUser[$(this).attr('id')] = $(this).val();
    });
    if ($("#test").prop("checked")) $addEditUser.test = "да";
    else $addEditUser.test = "нет";
    $addEditUser.sex = $("#sex").val();
    $addEditUser.place = $('input[name=place]:checked').val();

    $.ajax({ //запрос на сервер для изменения данных о пользователе
        type: 'PUT',
        url: '/api/user',
        data: $addEditUser //измененные данные о пользователе
    }).done(function () {
        backToRegistry();  //по окончании вернуться на главную страницу
    });
};

var validate = function () { //узнаем валидна true или невалидна false текущая форма
    var validity; //маркер валидности формы
    $("input[class='form-control']").each(function () { //Получаем данные, введенные пользователем
        if ($(this).val() == '') {          //если хотя бы одно поле пустое - форма невалидна
            $(this).addClass('invalid');
            validity = false;
        }
    });
    if (!$("#sex").val()) {             //если параметр не указан - форма невалидна
        $("#sex").addClass('invalid');
        validity = false;
    }
    if ($('input[name=place]:checked').val() == undefined) { //если параметр не указан - форма невалидна
        $('#place').addClass('invalid');
        validity = false;
    }
    if (validity == false) return validity;
    else validity = true;
    return validity;

};

//обработчик событий на изменение элементов формы,
//удаляет, если есть, дополнительные стили красного цвета для невалидных полей
var clearStyle = function (elem) {
    $(elem).removeClass("invalid")
};

