"use strict";

$.mockjax({  //перехват запроса на поиск совпадений по введенным данным  и ответ от фиктивного сервера
    url: "/api/search",
    type: 'POST',
    status: 200,
    contentType: 'json',
    response: function (settings) {

        var unparsedUsersString = localStorage.getItem("myKey");
        var users = unparsedUsersString ? JSON.parse(unparsedUsersString) : [];
        var $searchParameters = settings.data;

        var matchedUsers = users.filter(function (item) { // массив обЪектов совпадений в базе
            var match = true;
            for (var key in $searchParameters) { // проверяем введенные данные пользователя
                if ($searchParameters[key] != item[key]) { // Если введенное свойство отсутствует в базе
                    match = false; // у нас несовпадение, дальнейший поиск не имеет смысла
                    break; // ищем в следующей ячейке массива users
                }
            }
            return match;
        }); // массив обьектов совпадений в базе

        this.responseText = matchedUsers;
    }
});

$.mockjax({ //перехват запроса на редактирование данных пользователя и ответ от фиктивного сервера
    url: "/api/user/*",
    type: 'GET',
    status: 200,
    response: function (settings) {

        var unparsedUsersString = localStorage.getItem("myKey");
        var users = unparsedUsersString ? JSON.parse(unparsedUsersString) : [];
        var userId = settings.data.userId;
        var matchedUser;
        for (var i = 0; i < users.length; i++) { //перебираем входящий массив из обьектов
            if (userId == users[i]['id']) {      //находим id пользователя, подлежащего правке
                matchedUser = users[i];
            }
        }
        this.responseText = matchedUser;
    }
});

$.mockjax({  //перехват запроса на добавление нового пользователя
    url: "/api/user",
    type: 'POST',
    status: 200,
    response: function (settings) {

        var unparsedUsersString = localStorage.getItem("myKey");
        var users = unparsedUsersString ? JSON.parse(unparsedUsersString) : [];
        var $addEditUser = settings.data;

        users.push($addEditUser); //добавляем в массив введенные данные пользователя
        localStorage.setItem("myKey", JSON.stringify(users)); //обновляем localstorage
    }
});

$.mockjax({ //перехват запроса на правку существующего пользователя
    url: "/api/user",
    type: 'PUT',
    status: 200,
    response: function (settings) {
        var unparsedUsersString = localStorage.getItem("myKey");
        var users = unparsedUsersString ? JSON.parse(unparsedUsersString) : [];
        var $addEditUser = settings.data;
        for (var i = 0; i < users.length; i++) { //перебираем входящий массив из обьектов
            if ($addEditUser['id'] == users[i]['id']) users[i] = $addEditUser;   //Перезаписываем исправленного пользователя
        }
        localStorage.setItem("myKey", JSON.stringify(users)); //обновляем localstorage
    }
});

$.mockjax({ //перехват запроса на  удаление пользователя
    url: '/api/user/remove',
    type: 'DELETE',
    status: 200,
    response: function (settings) {
        var unparsedUsersString = localStorage.getItem("myKey");
        var users = unparsedUsersString ? JSON.parse(unparsedUsersString) : [];
        var parentInputId = settings.data;

        users.forEach(function (item, i) { // для всех элементов массива users
            if (item.id == parentInputId) { // сравниваем id позьователя и id
                // родителя кнопки удаления
                users.splice(i, 1); // по нахождении - удаляем элемент
            }
            localStorage.setItem("myKey", JSON.stringify(users));
        })

    }
});
