"use strict";

var dataFromServer = [{   //исходные данные пользователей, начальное состояние
    id: '469c2f6a-a9d0-4fd5-8818-f9fc21e6af39',
    firstName: "Henri",
    lastName: "Blacksmith",
    birthDate: "1999-11-13",
    test: "нет",
    sex: "Мужской",
    place: "Менеджер"
}, {
    id: '9615988e-4cee-47c7-8137-df764577dfa0',
    firstName: "Олег",
    lastName: "Ус",
    birthDate: "2001-11-11",
    test: "нет",
    sex: "Мужской",
    place: "Директор"
}, {
    id: 'e9805261-0df4-4f6b-bed2-6c861194891e',
    firstName: "Maria",
    lastName: "Silverbeard",
    birthDate: "2001-12-23",
    test: "да",
    sex: "Женский",
    place: "Работник"

}, {
    id: 'e789143f-c2e3-404c-a8de-2cfee53ad893',
    firstName: "Олег",
    lastName: "Усович",
    birthDate: "2002-02-22",
    test: "нет",
    sex: "Мужской",
    place: "Менеджер"
}, {
    id: '25946d63-4205-4582-bd04-4beb765eee27',
    firstName: "John",
    lastName: "Darkblood",
    birthDate: "2001-11-11",
    test: "да",
    sex: "Мужской",
    place: "Работник"
}, {
    id: '1ebb478a-8432-4a47-9519-29b47da5c3d4',
    firstName: "Олег",
    lastName: "Усовский",
    birthDate: "2004-03-12",
    test: "нет",
    sex: "Мужской",
    place: "Работник"
}

];
var users;  //принимает данные о пользоватяелях, передаваемых с сервера
var previousClick; //указывает, на каком элементе сделан клик
var direction; //указывает направление сортировки от а до я (strait) и от я до а (reverse)

// Если есть куки - подставить значения в поля ввода
(function addCookies() {
    var tmp = [];
    var tmp2 = [];
    var inCookie = {};
    var get = document.cookie;
    if (get != '') {
        tmp = (get.substr(0)).split('; '); // разделяем переменные
        for (var i = 0; i < tmp.length; i++) {
            tmp2 = tmp[i].split('='); // массив будет содержать
            inCookie[tmp2[0]] = tmp2[1]; // пары ключ(имя переменной)->значение
        }

        if (inCookie.firstName != undefined)
            $(document).ready(function () { // если параметр есть в куки - подставить
                $("#firstName").val(inCookie.firstName)
            });
        if (inCookie.lastName != undefined)
            $(document).ready(function () { // если параметр есть в куки - подставить
                $("#lastName").val(inCookie.lastName)
            });
        if (inCookie.birthDate != undefined)
            $(document).ready(function () { // если параметр есть в куки - подставить
                $("#birthDate").val(inCookie.birthDate)
            });
        if (inCookie.test != undefined)
            $(document).ready(function () { // если параметр есть в куки - подставить
                if (inCookie.test == 'да')
                    $("#test").prop('selectedIndex', 1);
                else
                    $("#test").prop('selectedIndex', 2)
            });
        if (inCookie.sex != undefined)
            $(document).ready(function () { // если параметр есть в куки - подставить
                if (inCookie.sex == 'Мужской')
                    $("#sex").prop('selectedIndex', 1);
                else
                    $("#sex").prop('selectedIndex', 2)
            });
        if (inCookie.place != undefined)
            $(document).ready(function () { // если параметр есть в куки - подставить
                if (inCookie.place == 'Директор')
                    $("#place").prop('selectedIndex', 1);
                else if (inCookie.place == 'Менеджер')
                    $("#place").prop('selectedIndex', 2);
                else
                    $("#place").prop('selectedIndex', 3);
            });
    }
})();

var loadData = function () { // функция загрузки сервера
    localStorage.setItem("myKey", JSON.stringify(dataFromServer)); // данные с сервера запишем в хранилище localStorage по ключу "myKey"
    $("#searchList").empty(); // очищаем окно поиска
    alert("Данные загружены");
};

var clearList = function () { // функция очиститки окна результатов поиска
    $("input[class='input_text']").each(function () {
        $("input[class='input_text']").val(""); // для всех полей ввода с классом input_text очищаем поля ввода
    });
    $("select.input_text").each(function () { // для всех select вернуть начальное значение
        $(this).prop('selectedIndex', 0)
    });
    $('input[type="checkbox"]').each(function () { // для всех checkbox убрать галочки
        $(this).prop("checked", false);
    });
    $("#searchList").empty(); // очищаем окно поиска

    searchUsers(); //выводим весь список пользователей
};

var searchUsers = function () { // функция поиска пользователей в базе
    var $searchParameters = {}; // объект с введёнными в поля поиска данным
    $(".input_text").each(function () { // Получаем данные, введенные пользователем
        $searchParameters[$(this).attr('id')] = $(this).val();
    });
    var toCookie = $searchParameters;
    for (var k in $searchParameters) { // удаляем пустые свойства, которые не были заполнены пользователем
        if ($searchParameters[k] === '') { // если пользователь ничего не ввёл $searchParameters - пустой массив
            delete $searchParameters[k];
        }
    }

    (function editCookies() { // данные в полях ввода переносим в куки
        if (Object.keys(toCookie).length != 0) {
            var tmp = [];
            var tmp2 = [];
            var inCookie = {};

            var get = document.cookie;
            if (get != '') {
                tmp = (get.substr(0)).split('; '); // разделяем переменные
                for (var i = 0; i < tmp.length; i++) {
                    tmp2 = tmp[i].split('='); // массив будет содержать  пары ключ(имя переменной)->значение
                    inCookie[tmp2[0]] = tmp2[1];
                }
            }
            for (var n in inCookie) { // удаляем старые куки
                document.cookie = n + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
            }
            for (var name in toCookie) { // Задаем новые куки с временем жизни 5 минут
                document.cookie = name + '=' + toCookie[name] + ';max-age=300';
            }
        }
    })();

    $.ajax({  //делаем запрос на сервер для поиска совпадений в базе согласно введенным данным
        type: 'POST',
        url: '/api/search',
        dataType: 'json',
        data: $searchParameters,  //передаем введенные данные
        success: function (response) { //сохраняем пришедший ответ в users
            users = response;
            if (!users || users.length === 0) {
                $("#searchList").empty(); // очищаем окно поиска
                $('#searchList').append( // дописываем в окно поиска фразу
                    'Результаты поиска отсутствуют');
            } else {
                previousClick = 0; //при поиске обнуляем значнеие предыдущего клика
                direction = 0; //обнуляем значение направления поиска
                sortList('div#firstNameButton.results_div'); // Первый поиск всегда по имени в алфавитном порядке
                printUser(users); // если пользователь ввел верные данные - вывести список совпадений
            }
        }
    });
};

var addUser = function () { // добавляем нового пользователя на новой страничке
    window.location = "add_edit_form.html";
};

var editUser = function (el) { // передаем id и редактируем пользователя на новой страничке
    var parentInputId = $(el).parent().parent().attr('id');
    window.location = "add_edit_form.html?userId=" + parentInputId;
};

var deleteUser = function (el) { // удаляем пользователя
    if (confirm("Вы действительно хотите удалить пользователя?")) { //делаем предварительный запрос на удалением
        var parentInputId = $(el).parent().parent().attr('id'); // ищем id родителя эллемента элемента, которому принадлежит
                                                                // кнопка, вызвавшая удаление
        $.ajax({ //Запрос на сервер на удаление пользователя
            type: 'DELETE',
            url: '/api/user/remove',
            data: parentInputId  //id пользователя, подлежащего удалению
        }).done(function () {
            $("#searchList").empty(); // очищаем окно поиска
            searchUsers(); //по новой выводим весь список пользователей
        });
    }
};

var sortList = function (el) {  //сортировка по алфавиту
    var whereClick = $(el).attr('id');   //определяем, на каком элементе был клик
    $('div.bar div').removeClass('triangle_up triangle_down'); //удалить старые стрелочки-направления сортировки, если есть
    if (previousClick != whereClick) {  //если предыдущий клик был по другому полю, проводим сортировку по текущему
        var compare = function (a, b) {//функция сравнения двух значений в алфавитном парадке
            var sortParameter;  //параметр по которому будет проводиться сортировка
            switch(whereClick) {
                case 'firstNameButton':
                    sortParameter = 'firstName';
                    break;
                case 'lastNameButton':
                    sortParameter = 'lastName';
                    break;
                case 'birthDateButton':
                    sortParameter = 'birthDate';
                    break;
                case 'testButton':
                    sortParameter = 'test';
                    break;
                case 'sexButton':
                    sortParameter = 'sex';
                    break;
                case 'placeButton':
                    sortParameter = 'place';
                    break;
            }
            a = a[sortParameter];
            b = b[sortParameter];
            return a.localeCompare(b);
        };
        direction = 'strait';  //указываем направление сортировки
        users.sort(compare);  //сортируем массив пользователей
        $(el).find('div').addClass('triangle_up'); //Установить фильтр вверх
    }
    else if (previousClick == whereClick) {  //если предыдущий клик был по этому же полю, сменить направление сортировки
        if (direction == 'strait') {
            direction = 'reverse';
            $(el).find('div').addClass('triangle_down');
        }
        else if (direction == 'reverse') {
            direction = 'strait';
            $(el).find('div').addClass('triangle_up');
        }
        users.reverse();
    }
    previousClick = whereClick;
    printUser(users);                  //вывести результаты
};

var printUser = function (print) { //вывод данных на экран
    $("#searchList").empty(); // очищаем окно поиска
    for (var i = 0; i < print.length; i++) { // перебираем входящий массив из обьектов
        var user = print[i];
        $('#searchList')
            .append(
                '<div id="'
                + user.id
                + '">'
                + '<div class="results_div">'
                + user.firstName
                + '</div>'
                + '<div class="results_div">'
                + user.lastName
                + '</div>'
                + '<div class="results_div">'
                + user.birthDate
                + '</div>'
                + '<div class="results_div_extra">'
                + user.test
                + '</div>'
                + '<div class="results_div_extra">'
                + user.sex
                + '</div>'
                + '<div class="results_div_extra">'
                + user.place
                + '</div>'
                + '<div class="results_div_extra"><input type="button" '
                + 'class="button_delete_edit" value="edit" onclick="editUser(this)"></div>'
                + '<div class="results_div_extra"><input type="button" '
                + 'class="button_delete_edit" value="X" onclick="deleteUser(this)"></div>'
                + // TODO this (хотя в даном случае может и не
                // получится убрать)
                '</div>')
    }
};

$(document).ready(function () { // по окончанию загрузки документа вывести список всех пользователей
    searchUsers();
});
